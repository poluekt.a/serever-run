DATAFILE=/root/deploy/airport-codes.csv
if ! [ -f "$DATAFILE" ]; then
 wget --quiet https://datahub.io/core/airport-codes/r/airport-codes.csv -O $DATAFILE;
fi

#if [ "$(docker network ls | grep bot_network)" == "" ]; then
  #docker network create bot_network
#fi

#cd /root/deploy

#docker run \
   # --rm \
   # --name=mydb \
    #--publish 3306:3306 \
    #--network bot_network \
    #--env MYSQL_ROOT_HOST='%' \
   # --env MYSQL_ROOT_PASSWORD=pass \
  #  --volume "$(pwd)/data:/var/lib/mysql" \
  #  --detach \
   # mysql:5.7 mysqld --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci

  #  while ! mysqladmin ping -h"127.0.0.1" --silent; do
  #  sleep 1
#done
#echo "DATABASE WITH AIRPORTS INFO IS READY"

#docker run \
  #  --rm \
   # --name bot \
   # --network bot_network \
  #  --volume "$PWD:/usr/src/myapp" \
  #  --workdir /usr/src/myapp \
  #  python:$(cat python-version.txt) \
 #   bash -c "python -m venv bot_venv && source bot_venv/bin/activate && pip install -r requirements.txt \
   #         && python -u db_init.py && python -u bot.py"

 # docker stop mydb

docker-compose down
docker-compose up