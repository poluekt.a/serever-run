import csv

# import docker as docker
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from MyTableClass import Airport
import requests
import io

#docker run --rm --name=mydb -p 3306:3306 --env MYSQL_ROOT_HOST='%' --env MYSQL_ROOT_PASSWORD=pass --volume "$(pwd)/data:/var/lib/mysql" mysql:5.7 mysqld --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
#db_host = '127.0.0.1'
CSV_URL = "https://datahub.io/core/airport-codes/r/airport-codes.csv"
db_host = 'mydb'

engine = create_engine(f"mysql://root:pass@{db_host}:3306/", echo=False)
engine.execute("create database if not exists Airports")
engine = create_engine(f"mysql://root:pass@{db_host}:3306/Airports?use_unicode=1&charset=utf8")

print("start of db import")

if not sqlalchemy.inspect(engine).has_table('airports'):
    Airport.metadata.create_all(engine)

    Session = sessionmaker(bind=engine)
    session = Session()

    with requests.Session() as s:
        decoded_content = s.get(CSV_URL).text
        csv_file = csv.DictReader(io.StringIO(decoded_content))
        count = 0
        for row in csv_file:
            print(row)
            # if (row != ):             next
            session.add(Airport(row))
            count += 1
    session.commit()

    print(count)
    print('end of db import')
#
# url_data = requests.Session()
# data_base = url_data.get(CSV_URL).content.decode('utf-8')
#
# cr = csv.reader(data_base.splitlines(), delimeter=',')
# csv_file = list(cr)
# #codes_file = open('airport-codes.csv', 'r')
# #csv_file = csv.DictReader(codes_file)
#
# count = 0
#
# for row in csv_file:
#     session.add(Airport(row))
#     count += 1
# session.commit()
#
# print(count)
# print('end of db import')

